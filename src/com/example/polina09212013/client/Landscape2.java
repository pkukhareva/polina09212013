package com.example.polina09212013.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.VerticalPanel;

public class Landscape2 extends Composite {
	private VerticalPanel vPanel2 =new VerticalPanel();
	private MainView main;

	public Landscape2 (MainView main){
		initWidget (this.vPanel2);
		this.main=main;
		Image landscape2 = new Image ("/Images/IMG2.JPG");
		landscape2.setWidth("600px");
		this.vPanel2.add(landscape2);
		Anchor anchor2 = new Anchor ("Go to landscape 1");
		anchor2.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				OpenLandscape1();
				
			}
		});
		this.vPanel2.add(anchor2);		
	}
	private void OpenLandscape1(){
		main.OpenLandscape1();
	}

}
