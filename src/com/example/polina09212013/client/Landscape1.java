package com.example.polina09212013.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.VerticalPanel;

public class Landscape1 extends Composite {
	private VerticalPanel vPanel1 =new VerticalPanel();
	private MainView main;
	public Landscape1 (MainView main){
		initWidget (this.vPanel1);
		this.main=main;
		Image landscape1 = new Image ("/Images/IMG1.JPG");
		landscape1.setWidth("600px");
		this.vPanel1.add(landscape1);
		Anchor anchor1 = new Anchor ("Go to landscape 2");
		anchor1.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				OpenLandscape2();
				
			}
		});
		this.vPanel1.add(anchor1);		
	}
	private void OpenLandscape2(){
		main.OpenLandscape2();
	}

}
